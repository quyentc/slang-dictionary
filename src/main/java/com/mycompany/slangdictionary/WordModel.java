package com.mycompany.slangdictionary;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordModel {
    private static final String WORD_FILE_NAME = "src/slang.txt";


    public static ArrayList<String> read(String absolutePath) {
        ArrayList<String> list = new ArrayList<String>();
        File file = new File(absolutePath);
        if (!file.exists()) {
            return list;
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            list = (ArrayList<String>) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }
    public static HashMap<String, String> readTextInJar(String path) {
        HashMap<String, String> map = new HashMap<String, String>();
        BufferedReader reader = null;
        InputStream inputStream = WordModel.class.getResourceAsStream(path);
        if (inputStream == null) {
            return new HashMap<String, String>();
        }
        try {
            reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line;
            String value;
            while ((line = reader.readLine()) != null) {
                String[] str = line.split("`");
                if (str.length > 1) {
                    value = map.put(str[0].trim(), str[1].trim());
                    if (value != null) {
                        String existValue = map.get(str[0].trim());
                        if (!existValue.equals(value.trim())) {
                            map.put(str[0].trim(), (existValue + "| " + value).trim());
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("readText: successful! File path: " + path);
        return map;
    }

    public static HashMap<String, String> readHashMap(String absolutePath) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        File file = new File(absolutePath);
        if (!file.exists()) {
            return hashMap;
        }
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            hashMap = (HashMap<String, String>) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            closeStream(ois);
        }
        System.out.println("readHashMap: successful! File path: " + absolutePath);
        return hashMap;
    }
    public static <K, V> void writeMap(Map<K, V> map, String absolutePath) {
        File file = new File(absolutePath);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(map);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeStream(fos);
            closeStream(oos);
        }
        System.out.println("writeMap: successful! File path: " + absolutePath);
    }

    public static <T> void write(List<T> list, String absolutePath) {
        File file = new File(absolutePath);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            if (file.exists()) {
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(list);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeStream(fos);
            closeStream(oos);
        }
        System.out.println("writeList: successful! File path: " + absolutePath);
    }

    /**
     * close input stream
     * 
     * @param is: input stream
     */
    private static void closeStream(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
 
    /**
     * close output stream
     * 
     * @param os: output stream
     */
    private static void closeStream(OutputStream os) {
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
