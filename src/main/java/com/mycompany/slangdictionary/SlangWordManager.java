package com.mycompany.slangdictionary;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class SlangWordManager implements WindowListener, KeyListener, ActionListener, ListSelectionListener {

	private HashMap<String, String> slDict = new HashMap<String, String>();
	private ArrayList<String> searchHistoryList = new ArrayList<String>();
	private final static String APP_NAME = "Slang Word";

	private final static String SLANG_PATH = "/slang.txt";
	private final static String HISTORY_PATH = "./slang-words/history_search.txt";
	private final static String SLANG_WORD_DICT_PATH = "./slang-words/slang_word_dict.txt";

	private final static String BUTTON_ON_THIS_DAY = "btnOTD";
	private final static String BUTTON_SEACH = "btnSearch";
	private final static String BUTTON_ADD = "btnAdd";
	private final static String BUTTON_EDIT = "btnEdit";
	private final static String BUTTON_DELETE = "btnDelete";
	private final static String BUTTON_CLEAR_HISTORY = "btnCLearHistory";

	private final static String MENU_ITEM_GUESS_KEY = "miGuessKey";
	private final static String MENU_ITEM_GUESS_DESC = "miGuessDesc";
	private final static String MENU_ITEM_RESET_ROOT_SW = "miReset";

	private final static String KEY = "Key";
	private final static String DESC = "Descripton";
	private final static String INPUT_LABEL = "Input";
	private final static String SEARCH_BUTTON_TEXT = "Search";

	private JFrame frame;
	private Container contentPane;
	private JTable tblResult;
	private DefaultTableModel tblModel;
	private JComboBox<String> cbSearchType;
	private JTextField tfSeach;
	private JLabel lbOTDSlangWord;
	private JList<String> lsSearchHistory;
	private DefaultListModel<String> historyListModel;
	private String game_key;
	private String game_desc;

	public void run() {
		init();
		loadSlangWordDictionary();
		createAndShowGUI();
		loadSlangWordOnTable();
		loadSearchHistory();
	}

	private void init() {
		File file = new File("./slang-words");
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	private void createAndShowGUI() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		frame = new JFrame(APP_NAME);
		contentPane = frame.getContentPane();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.addWindowListener(this);

		addMenuBar();
		addToolBar();
		addSearchPanel();
		addHistoryPanel();
		addModifyButtonPanel();

		/*
		 * Display frame
		 */
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/*
	 * Add menu bar
	 */
	private void addMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		JMenuItem miGuessKey = new JMenuItem("Guess Slangword by Key");
		JMenuItem miGuessDesc = new JMenuItem("Guess Slangword by Desc");
		JMenuItem miReset = new JMenuItem("Reset root slang word dictionary");

		miGuessKey.setActionCommand(MENU_ITEM_GUESS_KEY);
		miGuessDesc.setActionCommand(MENU_ITEM_GUESS_DESC);
		miReset.setActionCommand(MENU_ITEM_RESET_ROOT_SW);

		miGuessKey.addActionListener(this);
		miGuessDesc.addActionListener(this);
		miReset.addActionListener(this);

		menu.add(miGuessKey);
		menu.add(miGuessDesc);
		menu.add(miReset);
		menuBar.add(menu);
		frame.setJMenuBar(menuBar);
	}

	/*
	 * Add toolbar on this day slang word
	 */
	private void addToolBar() {
		JToolBar toolBar = new JToolBar();

		lbOTDSlangWord = new JLabel();
		onThisDaySlangWord();

		JButton btnOTD = new JButton("See another");
		btnOTD.setActionCommand(BUTTON_ON_THIS_DAY);
		btnOTD.addActionListener(this);

		toolBar.add(btnOTD);
		toolBar.addSeparator();
		toolBar.add(lbOTDSlangWord);

		contentPane.add(toolBar, BorderLayout.PAGE_START);
	}

	/*
	 * Add search panel
	 */
	private void addSearchPanel() {
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.Y_AXIS));

		JPanel inputSearchPanel = new JPanel();
		inputSearchPanel.setLayout(new BoxLayout(inputSearchPanel, BoxLayout.X_AXIS));
		inputSearchPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		JLabel lbSearch = new JLabel(INPUT_LABEL);
		lbSearch.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

		tfSeach = new JTextField(20);
		tfSeach.addKeyListener(this);

		String[] searchTypes = { KEY, DESC };
		cbSearchType = new JComboBox<String>(searchTypes);
		cbSearchType.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		JButton btnSearch = new JButton(SEARCH_BUTTON_TEXT);
		btnSearch.setActionCommand(BUTTON_SEACH);
		btnSearch.addActionListener(this);

		inputSearchPanel.add(lbSearch);
		inputSearchPanel.add(tfSeach);
		inputSearchPanel.add(cbSearchType);
		inputSearchPanel.add(btnSearch);

		String[] columnsName = { KEY, DESC };
		tblModel = new DefaultTableModel(columnsName, 0);
		tblResult = new JTable() {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			};
		};
		tblResult.setModel(tblModel);
		JScrollPane scrollResultPane = new JScrollPane(tblResult);

		searchPanel.add(inputSearchPanel);
		searchPanel.add(scrollResultPane, BorderLayout.CENTER);

		contentPane.add(searchPanel);
	}

	/*
	 * Add search history panel
	 */
	private void addHistoryPanel() {
		JPanel historyPanel = new JPanel();
		historyPanel.setLayout(new BoxLayout(historyPanel, BoxLayout.Y_AXIS));
		historyPanel.setPreferredSize(new Dimension(250, 400));

		JLabel lbHistory = new JLabel("Search History");
		lbHistory.setAlignmentX(Component.CENTER_ALIGNMENT);

		JButton btnClearHistory = new JButton("Clear");
		btnClearHistory.setActionCommand(BUTTON_CLEAR_HISTORY);
		btnClearHistory.addActionListener(this);
		btnClearHistory.setAlignmentX(Component.CENTER_ALIGNMENT);

		lsSearchHistory = new JList<String>();
		lsSearchHistory.addListSelectionListener(this);

		JScrollPane spHistory = new JScrollPane(lsSearchHistory);

		historyPanel.add(lbHistory);
		historyPanel.add(spHistory);
		historyPanel.add(btnClearHistory);

		contentPane.add(historyPanel, BorderLayout.EAST);
	}

	/*
	 * Add button panel
	 */
	private void addModifyButtonPanel() {
		JPanel buttunPanel = new JPanel();
		FlowLayout buttonLayout = new FlowLayout(FlowLayout.LEADING);
		buttunPanel.setLayout(buttonLayout);
		JButton btnAdd = new JButton("Add");
		JButton btnEdit = new JButton("Edit");
		JButton btnDelete = new JButton("Delete");

		btnAdd.setActionCommand(BUTTON_ADD);
		btnEdit.setActionCommand(BUTTON_EDIT);
		btnDelete.setActionCommand(BUTTON_DELETE);

		btnAdd.addActionListener(this);
		btnEdit.addActionListener(this);
		btnDelete.addActionListener(this);

		buttunPanel.add(btnAdd);
		buttunPanel.add(btnEdit);
		buttunPanel.add(btnDelete);

		contentPane.add(buttunPanel, BorderLayout.PAGE_END);
	}

	private void loadSlangWordDictionary() {
		long startTime = System.currentTimeMillis();
		slDict = WordModel.readHashMap(SLANG_WORD_DICT_PATH);
		if (slDict.size() == 0) {
			slDict = WordModel.readTextInJar(SLANG_PATH);
			WordModel.writeMap(slDict, SLANG_WORD_DICT_PATH);
		}
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Summary : load Dictionary time: " + elapsedTime + ". total slang words: " + slDict.size());
	}

	private void loadSlangWordOnTable() {
		long startTime = System.currentTimeMillis();
		tblModel = new DefaultTableModel();
		tblModel.addColumn("Key");
		tblModel.addColumn("Description");
		for (Map.Entry<String, String> entry : slDict.entrySet()) {
			String[] values = { entry.getKey(), entry.getValue() };
			tblModel.addRow(values);
		}
		tblResult.setModel(tblModel);
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Summary : load Dictionary to table time: " + elapsedTime + ". total slang words: " + slDict.size());
	}

	private void loadSearchHistory() {
		long startTime = System.currentTimeMillis();
		searchHistoryList = WordModel.read(HISTORY_PATH);
		historyListModel = new DefaultListModel<String>();
		for (String s : searchHistoryList) {
			historyListModel.addElement(s);
		}
		lsSearchHistory.setModel(historyListModel);
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(
				"Summary : load Dictionary time: " + elapsedTime + ". total search history words: " + searchHistoryList.size());
	}

	private HashMap<String, String> resetSlangWordDict() {
		slDict = WordModel.readTextInJar(SLANG_PATH);
		loadSlangWordOnTable();
		return slDict;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		long startTime = System.currentTimeMillis();
		switch (e.getActionCommand()) {
		case BUTTON_ON_THIS_DAY:
			onThisDaySlangWord();
			break;
		case BUTTON_SEACH:
			searchSlangWord();
			break;
		case BUTTON_ADD:
			addNewSlangWord();
			break;
		case BUTTON_EDIT:
			editSlangWord();
			break;
		case BUTTON_DELETE:
			deleteSlangWord();
			break;
		case BUTTON_CLEAR_HISTORY:
			clearHistorySearch();
			break;
		case MENU_ITEM_GUESS_KEY:
			guessSlangWordKey();
			break;
		case MENU_ITEM_GUESS_DESC:
			guessSlangWordDesc();
			break;
		case MENU_ITEM_RESET_ROOT_SW:
			resetSlangWord();
			break;
		default:
			break;
		}
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("actionPerformed time: " + elapsedTime);
	}



	private void onThisDaySlangWord() {
		if (slDict.size() > 0) {
			Random random = new Random();
			List<String> keys = new ArrayList<String>(slDict.keySet());
			int index = random.nextInt(keys.size());
			String key = keys.get(index);
			String value = slDict.get(key);
			lbOTDSlangWord.setText(key + ": " + value);
		} else {
			lbOTDSlangWord.setText("No slang word for today");
		}
	}

	private void searchSlangWord() {
		long startTime = System.currentTimeMillis();
		String type = (String) cbSearchType.getSelectedItem();
		String search_keyword = tfSeach.getText().trim();
		if (search_keyword.equals("")) {
			JOptionPane.showMessageDialog(frame, "Please input text!");
			return;
		}
		searchHistoryList.add(0, search_keyword);
		historyListModel.add(0, search_keyword);
		switch (type) {
		case KEY:
			String key = search_keyword;
			String value = slDict.get(key);
			if (value == null) {
				key = key.toLowerCase();
				value = slDict.get(key);
			}
			if (value == null) {
				key = key.toUpperCase();
				value = slDict.get(key);
			}
			tblModel.setRowCount(0);
			if (value != null && !value.equals("")) {
				String[] data = { key, value };
				tblModel.addRow(data);
			}
			break;
		case DESC:
			tblModel.setRowCount(0);
			String[] searchStrings = search_keyword.toLowerCase().split(" ");
			for (Map.Entry<String, String> item : slDict.entrySet()) {
				boolean flag = true;
				for (String s : searchStrings) {
					if (!item.getValue().toLowerCase().contains(s)) {
						flag = false;
						break;
					}
				}
				if (flag) {
					String[] data = { item.getKey(), item.getValue() };
					tblModel.addRow(data);
					System.out.println(item.getKey() + "-" + item.getValue());
				}
			}
			tblResult.setModel(tblModel);
			break;
		}
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("searchSlangWord time: " + elapsedTime);
	}

	private void addNewSlangWord() {
		JDialog addDialog = new JDialog(frame, "Add new slang word", true);
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		addDialog.setLayout(layout);
		JLabel lbAddKey = new JLabel("Key: ");
		JTextField tfAddKey = new JTextField(15);
		JLabel lbAddDesc = new JLabel("Description: ");
		JTextField tfAddDesc = new JTextField(15);
		JButton btnAddSave = new JButton("Save");
		JButton btnAddCancel = new JButton("Cancel");
		btnAddSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tfAddKey.getText().equals("")) {
					JOptionPane.showMessageDialog(addDialog, "Please input key");
					return;
				}
				if (tfAddDesc.getText().equals("")) {
					JOptionPane.showMessageDialog(addDialog, "Please input description");
					return;
				}
				String value = slDict.put(tfAddKey.getText().trim(), tfAddDesc.getText().trim());
				if (value != null) {
					String existKey = tfAddKey.getText().trim();
					String newValue = tfAddDesc.getText().trim();
					String[] options = { "Duplicate", "Override", "Cancel" };
					int option = JOptionPane.showOptionDialog(addDialog,
							"The slang word you input already exist. Do you want to duplicate or override it?",
							"Duplicate slang word", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
							options, options[2]);
					// Duplicate
					if (option == 0) {
						slDict.put(existKey, value + " | " + newValue);
					}
					// Override
					else if (option == 1) {
						slDict.put(existKey, newValue);
					}
					// Cancel
					else if (option == 2) {
						return;
					}
				}
				loadSlangWordOnTable();
				addDialog.dispose();

			}
		});
		btnAddCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addDialog.dispose();
			}
		});

		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		addDialog.add(lbAddKey, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		addDialog.add(tfAddKey, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		addDialog.add(lbAddDesc, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		addDialog.add(tfAddDesc, gbc);

		JPanel bp = new JPanel();
		bp.add(btnAddSave);
		bp.add(btnAddCancel);

		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		addDialog.add(bp, gbc);

		addDialog.setSize(new Dimension(400, 200));
		addDialog.setLocationRelativeTo(null);
		addDialog.setVisible(true);
	}

	private void editSlangWord() {
		if (tblResult.getSelectedRows().length > 1) {
			JOptionPane.showMessageDialog(frame, "Please choose only one row to edit!");
			return;
		}
		int selectedRow = tblResult.getSelectedRow();
		if (selectedRow < 0) {
			JOptionPane.showMessageDialog(frame, "Please choose a row to edit!");
			return;
		}
		String key = (String) tblResult.getValueAt(selectedRow, 0);
		String value = (String) tblResult.getValueAt(selectedRow, 1);

		JDialog editDialog = new JDialog(frame, "Edit slang word", true);
		GridBagLayout editLayout = new GridBagLayout();
		GridBagConstraints editGBC = new GridBagConstraints();

		editGBC.fill = GridBagConstraints.HORIZONTAL;
		editDialog.setLayout(editLayout);

		JLabel lbEditKey = new JLabel("Key: ");
		JLabel lbEditDesc = new JLabel("Description: ");
		JTextField tfEditKey = new JTextField(15);
		JTextField tfEditDesc = new JTextField(15);

		tfEditKey.setText(key);
		tfEditKey.setEditable(false);
		tfEditDesc.setText(value);

		JButton btnEditSave = new JButton("Save");
		JButton btnEditCancel = new JButton("Cancel");
		btnEditSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tfEditDesc.getText().equals("")) {
					JOptionPane.showMessageDialog(editDialog, "Please input description");
					return;
				}
				slDict.put(key, tfEditDesc.getText().trim());
				loadSlangWordOnTable();
				editDialog.dispose();

			}
		});
		btnEditCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editDialog.dispose();
			}
		});

		editGBC.gridx = 0;
		editGBC.gridy = 0;
		editGBC.gridwidth = 1;
		editDialog.add(lbEditKey, editGBC);

		editGBC.gridx = 1;
		editGBC.gridy = 0;
		editGBC.gridwidth = 2;
		editDialog.add(tfEditKey, editGBC);

		editGBC.gridx = 0;
		editGBC.gridy = 1;
		editGBC.gridwidth = 1;
		editDialog.add(lbEditDesc, editGBC);

		editGBC.gridx = 1;
		editGBC.gridy = 1;
		editGBC.gridwidth = 2;
		editDialog.add(tfEditDesc, editGBC);

		JPanel editBtnPanel = new JPanel();
		editBtnPanel.add(btnEditSave);
		editBtnPanel.add(btnEditCancel);

		editGBC.gridx = 0;
		editGBC.gridy = 2;
		editGBC.gridwidth = GridBagConstraints.REMAINDER;
		editDialog.add(editBtnPanel, editGBC);

		editDialog.setSize(new Dimension(400, 200));
		editDialog.setLocationRelativeTo(null);
		editDialog.setVisible(true);
	}

	private void deleteSlangWord() {
		int[] deleteSelectedRows = tblResult.getSelectedRows();
		if (deleteSelectedRows.length <= 0) {
			JOptionPane.showMessageDialog(frame, "Please choose a row to delete!");
			return;
		}
		int option_delete = JOptionPane.showConfirmDialog(frame, "Do you want to delete selected items?",
				"Delete slang word", JOptionPane.YES_NO_OPTION);
		if (option_delete == 0) {
			for (int i : deleteSelectedRows) {
				String delete_key = (String) tblResult.getValueAt(i, 0);
				slDict.remove(delete_key);
			}
			loadSlangWordOnTable();
		}
	}

	private void clearHistorySearch() {
		searchHistoryList.clear();
		historyListModel.removeAllElements();
	}

	private void guessSlangWordKey() {
		ArrayList<String> keys = randomKeys();
		ArrayList<String> descList = descList(keys);

		JDialog guessKeyDialog = new JDialog(frame, "Guess key of slang word", true);
		GridLayout guessKeyLayout = new GridLayout(6, 1);
		guessKeyDialog.setLayout(guessKeyLayout);
		guessKeyDialog.setSize(400, 250);

		JRadioButton radio1 = new JRadioButton(keys.get(0));
		JRadioButton radio2 = new JRadioButton(keys.get(1));
		JRadioButton radio3 = new JRadioButton(keys.get(2));
		JRadioButton radio4 = new JRadioButton(keys.get(3));

		ButtonGroup rdoGroup = new ButtonGroup();
		rdoGroup.add(radio1);
		rdoGroup.add(radio2);
		rdoGroup.add(radio3);
		rdoGroup.add(radio4);

		JLabel lbKey = new JLabel("Which slang word mean " + descList.get(0) + " ?");
		JButton btnGuess = new JButton("Result");
		btnGuess.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean result = false;
				if (radio1.isSelected()) {
					result = gameResult(radio1.getText(), game_desc, guessKeyDialog);
				} else if (radio2.isSelected()) {
					result = gameResult(radio2.getText(), game_desc, guessKeyDialog);
				} else if (radio3.isSelected()) {
					result = gameResult(radio3.getText(), game_desc, guessKeyDialog);
				} else if (radio4.isSelected()) {
					result = gameResult(radio4.getText(), game_desc, guessKeyDialog);
				}
				if (result) {
					int continueOption = JOptionPane.showConfirmDialog(guessKeyDialog,
							"You're correct! Do you want to continue?", "Bravo!", JOptionPane.YES_NO_OPTION);
					if (continueOption == 0) {
						ArrayList<String> keys = randomKeys();
						descList(keys);
						radio1.setText(keys.get(0));
						radio2.setText(keys.get(1));
						radio3.setText(keys.get(2));
						radio4.setText(keys.get(3));
						lbKey.setText("Which slang word mean " + game_desc + " ?");
					}
				}
			}
		});

		guessKeyDialog.add(lbKey);
		guessKeyDialog.add(radio1);
		guessKeyDialog.add(radio2);
		guessKeyDialog.add(radio3);
		guessKeyDialog.add(radio4);
		guessKeyDialog.add(btnGuess);
		guessKeyDialog.setLocationRelativeTo(null);
		guessKeyDialog.setVisible(true);
	}

	private void guessSlangWordDesc() {

		ArrayList<String> descList = descList(randomKeys());

		JDialog guessDescDialog = new JDialog(frame, "Guess description of slang word", true);
		GridLayout guessDescLayout = new GridLayout(6, 1);
		guessDescDialog.setLayout(guessDescLayout);
		guessDescDialog.setSize(400, 250);

		JRadioButton radio1 = new JRadioButton(descList.get(0));
		JRadioButton radio2 = new JRadioButton(descList.get(1));
		JRadioButton radio3 = new JRadioButton(descList.get(2));
		JRadioButton radio4 = new JRadioButton(descList.get(3));

		ButtonGroup rdoGroup = new ButtonGroup();
		rdoGroup.add(radio1);
		rdoGroup.add(radio2);
		rdoGroup.add(radio3);
		rdoGroup.add(radio4);

		JLabel lbKey = new JLabel("What does " + game_key + " mean?");
		JButton btnGuess = new JButton("Result");
		btnGuess.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				boolean result = false;
				if (radio1.isSelected()) {
					result = gameResult(game_key, radio1.getText(), guessDescDialog);
				} else if (radio2.isSelected()) {
					result = gameResult(game_key, radio2.getText(), guessDescDialog);
				} else if (radio3.isSelected()) {
					result = gameResult(game_key, radio3.getText(), guessDescDialog);
				} else if (radio4.isSelected()) {
					result = gameResult(game_key, radio4.getText(), guessDescDialog);
				}
				if (result) {
					int continueOption = JOptionPane.showConfirmDialog(guessDescDialog,
							"You're correct! Do you want to continue?", "Bravo!", JOptionPane.YES_NO_OPTION);
					if (continueOption == 0) {
						ArrayList<String> descList = descList(randomKeys());
						radio1.setText(descList.get(0));
						radio2.setText(descList.get(1));
						radio3.setText(descList.get(2));
						radio4.setText(descList.get(3));
						lbKey.setText("What does " + game_key + " mean?");
					}
				}
			}
		});

		guessDescDialog.add(lbKey);
		guessDescDialog.add(radio1);
		guessDescDialog.add(radio2);
		guessDescDialog.add(radio3);
		guessDescDialog.add(radio4);
		guessDescDialog.add(btnGuess);
		guessDescDialog.setLocationRelativeTo(null);
		guessDescDialog.setVisible(true);
	}

	private ArrayList<String> randomKeys() {
		Random random = new Random();
		List<String> keys = new ArrayList<String>(slDict.keySet());
		String key2, key3, key4;
		do {
			game_key = keys.get(random.nextInt(keys.size()));
			key2 = keys.get(random.nextInt(keys.size()));
			key3 = keys.get(random.nextInt(keys.size()));
			key4 = keys.get(random.nextInt(keys.size()));
		} while (validateDuplicateKey(game_key, key2, key3, key4));
		return new ArrayList<String>(List.of(game_key, key2, key3, key4));
	}

	private ArrayList<String> descList(ArrayList<String> keys) {
		ArrayList<String> descList = new ArrayList<String>();
		for (String key : keys) {
			descList.add(slDict.get(key));
		}
		Collections.shuffle(descList);
		game_desc = descList.get(0);
		return descList;
	}

	private boolean validateDuplicateKey(String key1, String key2, String key3, String key4) {
		if (key1.equals(key2) || key1.equals(key3) || key1.equals(key4)) {
			return true;
		}
		if (key2.equals(key3) || key2.equals(key4)) {
			return true;
		}
		if (key3.equals(key4)) {
			return true;
		}
		return false;
	}

	private boolean gameResult(String key, String desc, Component dialog) {
		if (slDict.get(key).equals(desc)) {
			return true;
		} else {
			JOptionPane.showMessageDialog(dialog, "You're wrong!", "Opps!", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}

	private void resetSlangWord() {
		int option_reset = JOptionPane.showConfirmDialog(frame, "Are you sure to reset root slang word?",
				"Reset root slang word", JOptionPane.YES_NO_OPTION);
		if (option_reset == 0) {
			resetSlangWordDict();
			System.out.println("Reset root slang word successful");
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		tfSeach.setText(lsSearchHistory.getSelectedValue());
	}

	@Override
	public void windowOpened(WindowEvent e) {
		System.out.println("windowOpened");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		WordModel.writeMap(slDict, SLANG_WORD_DICT_PATH);
		WordModel.write(searchHistoryList, HISTORY_PATH);
		System.out.println("windowClosing");
	}

	@Override
	public void windowClosed(WindowEvent e) {
		System.out.println("windowClosed");
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (tfSeach.getText().equals("")) {
			loadSlangWordOnTable();
		}
	}
}
