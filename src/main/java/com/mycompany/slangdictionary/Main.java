package com.mycompany.slangdictionary;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        SlangWordManager app = new SlangWordManager();
        app.run();
    }
}
